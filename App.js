import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ForgotPasswordScreen from './screens/ForgotPasswordScreen';
import ProductsScreen from './screens/ProductsScreen';
import ProductDetailScreen from './screens/ProductDetailScreen';



export default function App() {
  const Stack = createStackNavigator();
  const GlobalOptions= {
    headerStyle: {
      backgroundColor: '#35317E',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }
  const hideHeaderHandler = ()=>({
    headerShown:false
  })
  return (
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Home" screenOptions={GlobalOptions}>
      <Stack.Screen name="Home" component={HomeScreen} options={hideHeaderHandler} />
      <Stack.Screen name="Login" component={LoginScreen} options={hideHeaderHandler}  />
      <Stack.Screen name="Register" component={RegisterScreen} options={hideHeaderHandler}  />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} options={hideHeaderHandler}  />
      <Stack.Screen name="Products" component={ProductsScreen}  />
      <Stack.Screen name="ProductDetail" component={ProductDetailScreen}  />
      {/* <Stack.Screen name="Products" component={ProductsScreen} /> */}
    </Stack.Navigator>
  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

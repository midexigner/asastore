import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Copyright = () => {
    return (
        <View style={styles.container}>
          <Text style={styles.title}>copyright {new Date().getFullYear()}, ASA</Text>
        </View>
    )
}

export default Copyright

const styles = StyleSheet.create({
    container:{
        justifyContent:'center',
        alignItems:'center',
        position:'absolute',
        bottom:2,
        width:'100%',
    },
    title:{
        color:'#fff',
        textAlign:'center',
        textTransform:'uppercase'
    }
})

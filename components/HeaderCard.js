import React from 'react'
import {  StyleSheet, Image, View,Dimensions } from 'react-native'

const HeaderCard = () => {
    return (
        <View style={styles.header}>
        <Image source={require('../assets/logo.png')} />
    </View>
    )
}

export default HeaderCard

const styles = StyleSheet.create({
    header:{
        width:Dimensions.get('window').width,
        height:100,
        backgroundColor:'#080942',
        justifyContent:'center',
        alignItems:'center'

    }
})

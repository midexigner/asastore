import React from 'react'
import { StyleSheet, } from 'react-native'
import { Card, Button,Text } from 'react-native-elements';
const ProductCard = ({navigation}) => {
    return (
        <Card.Image source={{uri: 'https://vader-prod.s3.amazonaws.com/1543958419-810KAtkwn6L.jpg'}}>
        <Text style={{marginBottom: 10, marginTop: 20 }} h2>
            Kid shoes
        </Text>
        <Text style={styles.price} h4>
            $ 200
        </Text>
        <Text h6 style={styles.description}>
            added 2h ago
        </Text>
        <Button
        type="clear"
        title='Buy now'
        onPress={() => navigation.navigate('ProductDetail')}
        />
    </Card.Image>
    )
}

export default ProductCard

const styles = StyleSheet.create({
    name: {
        color: '#5a647d',
        fontWeight: 'bold',
        fontSize: 30
    },
    price: {
        fontWeight: 'bold',
        marginBottom: 10
    },
    description: {
        fontSize: 10,
        color: '#c1c4cd'
    }
})

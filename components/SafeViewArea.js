import React from 'react'
import { StyleSheet, View,Platform,SafeAreaView  } from 'react-native'
import { StatusBar } from 'expo-status-bar';
const SafeViewArea = ({children}) => {
    return (
        <SafeAreaView  style={styles.droidSafeArea}>
             <StatusBar style="light" />
           {children}
        </SafeAreaView>
    )
}

export default SafeViewArea

const styles = StyleSheet.create({
    droidSafeArea: {
        flex: 1,
        paddingTop: Platform.OS === 'android' ? 25 : 0,
        backgroundColor:'#35317E'
    },
})

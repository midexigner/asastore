import React from 'react'
import { StyleSheet, Text, View,Dimensions,TouchableHighlight } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
const Title = ({title,isBack,navigation}) => {
    return (
        <View style={styles.titlehead}>
             {isBack &&
             <TouchableHighlight onPress={() => navigation.goBack()}  style={styles.back}>
             <AntDesign name="left" size={24} color="white" />
            </TouchableHighlight>}
            <Text style={styles.title}>{title}</Text>
        </View>
    )
}

export default Title

const styles = StyleSheet.create({
    titlehead:{
    width:Dimensions.get('window').width,
    height:55,
    backgroundColor:'#231C69',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'
    },
    title:{
        textAlign:'center',
        textTransform:'uppercase',
        fontSize:20,
        fontWeight:'bold',
        color:'#fff'
    },
    back:{
        position:'absolute',
        left:20
    }
})

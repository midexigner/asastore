import React,{useState} from 'react'
import { StyleSheet, Text, ImageBackground,View, TextInput,
    TouchableHighlight,Platform, KeyboardAvoidingView  } from 'react-native'
import HeaderCard from '../components/HeaderCard'
import SafeViewArea from '../components/SafeViewArea'
import Title from '../components/Title'
import { FontAwesome } from "@expo/vector-icons";
import Copyright from '../components/Copyright'

const ForgotPasswordScreen = ({ navigation }) => {
    const [input,setInput] = useState("");

    const handlerForget = ()=>{
        console.log(input);
        navigation.navigate('Login')
        setInput('');
    }
    return (
        <SafeViewArea>
              <HeaderCard />
      <Title title="Forgot password" />
      <ImageBackground source={require("../assets/background.png")}
        style={styles.bgImage}>
 <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}   keyboardVerticalOffset={85} style={styles.body}>
          <View>
            <View>
            <Text>Forgot your password?</Text>
            <Text>To recover your password, you need to enter your registered email address. We will sent the recovery code to your email</Text>
            </View>
            <View>
              <View style={styles.inputBox}>
                <FontAwesome name="envelope" size={24} style={styles.inputIcon} />
              <TextInput 
              style={styles.input} 
              placeholderTextColor="#3A3A3A"
              placeholder={'Type your Email'}
              textContentType={'emailAddress'} 
              keyboardType={'email-address'}
              autoCompleteType={'email'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'NEXT'}
              returnKeyType={'next'}
              onSubmitEditing={handlerForget}
              value={input}
              onChangeText={(text)=>setInput(text)}
               />
              </View>
            </View>

            <TouchableHighlight
              style={styles.buttonBG}
              onPress={handlerForget}
            >
              <Text style={styles.label}>Send</Text>
            </TouchableHighlight>
        
         
          </View>
          <Copyright />
        </KeyboardAvoidingView>
        </ImageBackground>
          
        </SafeViewArea>
    )
}

export default ForgotPasswordScreen

const styles = StyleSheet.create({
    bgImage:{
        resizeMode: "contain",
        backgroundColor: "#35317E",
        width: "100%",
        flex:1,
    },
    body: {
        flex:1,
        alignContent: "space-between",
        justifyContent: "space-between",
        marginLeft: 20,
        marginRight: 20,
        marginVertical: 49,
      },
      inputBox:{
        height: 45,
        backgroundColor: "rgba(255,255,255,1)",
        flexDirection: 'row',
        alignItems:'center',
        width:'100%',
        paddingHorizontal:10
      },
      inputIcon:{
      color:'rgba(0,0,0,.22)',
      },
      input: {
        flex:1,
        paddingLeft: 15,
        height: 40,
        borderBottomColor: "rgba(255,255,255,0)",
        borderBottomWidth: 0,
        color:'#3a3a3a',
        
      },
      label: {
        textAlign: "left",
        textTransform: "uppercase",
        fontSize: 16,
        fontWeight: "500",
        color: "#fff",
      },
      buttonBG: {
        width: "100%",
        height: 55,
        backgroundColor: "#20ABBD",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginRight: 40,
        borderRadius:999
      },
      
})

import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeTabScreen from './home/HomeTabScreen'
import UserProfileScreen from './UserProfileScreen'
import {AntDesign } from '@expo/vector-icons'; 

const HomeScreen = ({navigation}) => {
    const Tab = createBottomTabNavigator();
    return (
      <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#ffffff',
        inactiveTintColor: '#ffffff',
        activeBackgroundColor:"#EC07A1",
        inactiveBackgroundColor:"#431C75",
        style:{height:70},
        labelStyle: {
					fontSize: 12,
				},
      }}
      >
        <Tab.Screen name="HomeTab" component={HomeTabScreen} 
        options={{
            tabBarLabel: 'Home',
            tabBarIcon: () => (
              <AntDesign name="home" size={24} color="#ffffff" />
            ),
          }}  />
        <Tab.Screen name="UserProfile" component={UserProfileScreen} options={{
          tabBarLabel: 'Profile',
          tabBarIcon: () => (
            <AntDesign name="user" size={24} color="#ffffff" />
          ),
        }} />
      </Tab.Navigator>
    )
}

export default HomeScreen


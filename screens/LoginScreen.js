import React, { useState } from 'react'
import { StyleSheet, Text, ImageBackground,View, TextInput,
    TouchableHighlight,TouchableWithoutFeedback,KeyboardAvoidingView,Platform } from 'react-native'
import Title from '../components/Title'
import HeaderCard from '../components/HeaderCard'
import SafeViewArea from '../components/SafeViewArea'
import { FontAwesome,EvilIcons } from "@expo/vector-icons";
import Copyright from '../components/Copyright'

const LoginScreen = ({navigation}) => {
  const [name,setName] = useState('');
  const [password,setPassword] = useState('');
  const handlerLogin = ()=>{
    console.log(name);
    console.log(password);
    navigation.navigate('HomeTab')
    setName('')
    setPassword('')
  }
    return (
        <SafeViewArea>
             <HeaderCard />
            <Title title="Sign In" />
            <ImageBackground source={require("../assets/background.png")}
        style={styles.bgImage}>
           <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"}   keyboardVerticalOffset={85} style={styles.body}>
          <View>
            <View>
              <Text style={styles.label}>Username</Text>
              <View style={styles.inputBox}>
                <FontAwesome name="user" size={24} style={styles.inputIcon} />
              <TextInput 
              style={styles.input} 
              placeholderTextColor="#fff"
              placeholder={'Demoaccount123'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'NEXT'}
              returnKeyType={'next'}
              value={name}
              onChangeText={(text)=>setName(text)}
               />
              </View>
            </View>
            <View style={{ marginTop: 30 }}>
              <Text style={styles.label}>Password</Text>
              <View style={styles.inputBox}>
                <EvilIcons name="lock" size={30} style={styles.inputIcon} />
              <TextInput 
              secureTextEntry={true} 
              style={styles.input}
              placeholderTextColor="#fff"
              placeholder={'**************'}
              textContentType={'password'}
              autoCompleteType={'password'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'LOGIN'}
              returnKeyType={'done'}
              value={password}
              onChangeText={(text)=>setPassword(text)}
              onSubmitEditing={handlerLogin}
               />
              </View>
            </View>

            <TouchableHighlight
              style={styles.buttonBG}
              onPress={handlerLogin}
            >
              <Text style={styles.label}>Secure Login</Text>
            </TouchableHighlight>
           
            <View style={styles.textBox}>
            <TouchableWithoutFeedback
              style={styles.btn2}
              onPress={() => navigation.navigate('ForgotPassword')}
            >
              <Text style={styles.btn2label}>Forgot Password?</Text>
            </TouchableWithoutFeedback>
          
              <Text style={styles.btn3label}>Don't have an account? <TouchableWithoutFeedback 
              onPress={() => navigation.navigate('Register')}
            ><Text style={styles.btn3Link}>sign up now</Text>
            </TouchableWithoutFeedback></Text>
          </View>
         
          </View>
          <Copyright />
        </KeyboardAvoidingView>
            </ImageBackground>
        </SafeViewArea>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    bgImage:{
        resizeMode: "contain",
        backgroundColor: "#35317E",
        width: "100%",
        flex:1,
    },
    body: {
        flex:1,
        alignContent: "space-between",
        justifyContent: "space-between",
        marginLeft: 20,
        marginRight: 20,
        marginVertical: 49,
      },
      label: {
        textAlign: "left",
        textTransform: "uppercase",
        fontSize: 16,
        fontWeight: "500",
        color: "#fff",
      },
      inputBox:{
        height: 40,
        borderBottomColor: "rgba(255,255,255,.23)",
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems:'center',
        width:'100%',
      },
      inputIcon:{
      color:'#fff',
      },
      input: {
        flex:1,
        paddingLeft: 15,
        height: 40,
        borderBottomColor: "rgba(255,255,255,0)",
        borderBottomWidth: 0,
        color:'#fff',
        
      },
      buttonBG: {
        width: "100%",
        height: 55,
        backgroundColor: "#20ABBD",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginRight: 40,
        borderRadius:999
      },
      textBox:{
        textAlign: "center",
        marginTop: 20,
      },
      btn2:{
        marginTop: 30,
      },
      btn2label: {
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "500",
        color: "#fff",
      },
      btn3:{
        marginTop: 20,
      },
      btn3label: {
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "700",
        color: "#fff",
        marginTop:35,
        position:'relative'
      },
      btn3Link:{
        padding:0,
        margin:0,
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "700",
        color: "#FB7DFF",
        position:'relative',
        top:2

      },
      
})

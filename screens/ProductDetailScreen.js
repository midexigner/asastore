import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-elements'
import HeaderCard  from '../components/HeaderCard'
import SafeViewArea from '../components/SafeViewArea'

const ProductDetailScreen = ({ navigation,route}) => {
    return (
        <SafeViewArea>
           <HeaderCard />
           <Text style={{color:"#fff"}}>{route.params.Name}</Text>
            <Text>I am Product Detail Screen</Text>
            <Button title="goback detail"  onPress={() => navigation.navigate('Products')} />
        </SafeViewArea>
    )
}

export default ProductDetailScreen

const styles = StyleSheet.create({})

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import SafeViewArea from '../components/SafeViewArea'

const ProductsScreen = ({ navigation }) => {
    return (
        <SafeViewArea>
            <Text>I am a Products Screen</Text>
        </SafeViewArea>
    )
}

export default ProductsScreen

const styles = StyleSheet.create({})

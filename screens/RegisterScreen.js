import React from 'react'
import { StyleSheet, Text, ImageBackground,View, TextInput,
    TouchableHighlight,TouchableWithoutFeedback  } from 'react-native'
import HeaderCard from '../components/HeaderCard'
import SafeViewArea from '../components/SafeViewArea'
import Title from '../components/Title'
import { FontAwesome,EvilIcons } from "@expo/vector-icons";
import Copyright from '../components/Copyright'


const RegisterScreen = ({ navigation }) => {
    return (
        <SafeViewArea>
              <HeaderCard />
            <Title title="Register" />
            <ImageBackground source={require("../assets/background.png")}
        style={styles.bgImage}>
 <View style={styles.body}>
          <View>
            <View>
              <Text style={styles.label}>Username</Text>
              <View style={styles.inputBox}>
                <FontAwesome name="user" size={24} style={styles.inputIcon} />
              <TextInput 
              style={styles.input} 
              placeholderTextColor="#fff"
              placeholder={'Demoaccount123'}
              autoCompleteType={'email'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'NEXT'}
              returnKeyType={'next'}
               />
              </View>
            </View>
            <View style={{ marginTop: 30 }}>
              <Text style={styles.label}>Email</Text>
              <View style={styles.inputBox}>
                <FontAwesome name="envelope" size={24} style={styles.inputIcon} />
              <TextInput 
              style={styles.input} 
              placeholderTextColor="#fff"
              placeholder={'Demoaccount123'}
              textContentType={'emailAddress'} 
              keyboardType={'email-address'}
              autoCompleteType={'email'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'NEXT'}
              returnKeyType={'next'}
               />
              </View>
            </View>
            <View style={{ marginTop: 30 }}>
              <Text style={styles.label}>Password</Text>
              <View style={styles.inputBox}>
                <EvilIcons name="lock" size={30} style={styles.inputIcon} />
              <TextInput 
              secureTextEntry={true} 
              style={styles.input}
              placeholderTextColor="#fff"
              placeholder={'**************'}
              textContentType={'password'}
              autoCompleteType={'password'}
              clearButtonMode={'while-editing'}
              returnKeyLabel={'LOGIN'}
              returnKeyType={'done'}
               />
              </View>
            </View>

            <TouchableHighlight
              style={styles.buttonBG}
              onPress={() => alert("Pressed!")}
            >
              <Text style={styles.label}>Sign Me Up</Text>
            </TouchableHighlight>
           
            <View>
              <Text style={styles.btn3label}>Don't have an account? <TouchableWithoutFeedback 
              onPress={() => navigation.navigate('Login')}
            ><Text style={styles.btn3Link}>sign In</Text>
            </TouchableWithoutFeedback></Text>
          </View>
         
          </View>
          <Copyright />
        </View>
        </ImageBackground>
        </SafeViewArea>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({
    bgImage:{
        resizeMode: "contain",
        backgroundColor: "#35317E",
        width: "100%",
        flex:1,
    },
    body: {
        flex:1,
        alignContent: "space-between",
        justifyContent: "space-between",
        marginLeft: 20,
        marginRight: 20,
        marginVertical: 49,
      },
      label: {
        textAlign: "left",
        textTransform: "uppercase",
        fontSize: 16,
        fontWeight: "500",
        color: "#fff",
      },
      inputBox:{
        height: 40,
        borderBottomColor: "rgba(255,255,255,.23)",
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems:'center',
        width:'100%',
      },
      inputIcon:{
      color:'#fff',
      },
      input: {
        flex:1,
        paddingLeft: 15,
        height: 40,
        borderBottomColor: "rgba(255,255,255,0)",
        borderBottomWidth: 0,
        color:'#fff',
        
      },
      buttonBG: {
        width: "100%",
        height: 55,
        backgroundColor: "#20ABBD",
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20,
        marginRight: 40,
        borderRadius:999
      },
      textBox:{
        textAlign: "center",
        marginTop: 20,
      },
      btn2:{
        marginTop: 20,
      },
      btn2label: {
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "500",
        color: "#fff",
      },
      btn3:{
        marginTop: 20,
      },
      btn3label: {
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "700",
        color: "#fff",
        marginTop:35,
        position:'relative'
      },
      btn3Link:{
        padding:0,
        margin:0,
        textAlign: "center",
        textTransform: "uppercase",
        fontSize: 14,
        fontWeight: "700",
        color: "#FB7DFF",
        position:'absolute',
        top:4

      },
})

import React from 'react'
import { StyleSheet, Text, View,ImageBackground,Dimensions } from 'react-native'
import SafeViewArea from '../components/SafeViewArea'
import HeaderCard  from '../components/HeaderCard'
import Title from '../components/Title'
import { Avatar,Button } from 'react-native-elements';

const UserProfileScreen = ({navigation}) => {
    return (
        <SafeViewArea>
            <HeaderCard />
            <Title title="Robert Alex" />
            <ImageBackground source={require("../assets/background.png")}
        style={styles.bgImage}>
           <View style={styles.profileSec}>
           <Avatar
            source={{uri:'https://secure.gravatar.com/avatar/3745990a425cf002fc133e48d120a397?s=800&d=mm&r=g',}}
            size="large"
            rounded
            onPress={() => console.log("Works!")}
            activeOpacity={0.7}
/> 
<Text style={styles.profileTitle}>Robert Alex</Text>
<Text style={styles.profileSubTitle}>Software Product Designer</Text>
           </View>
<View style={styles.listing}>
   <View style={styles.listItem}>
       <Text style={styles.listTitle}>Phone:</Text>
       <Text style={styles.listTitle}>1111 -123 -1324</Text>
       </View> 
   <View style={styles.listItem}>
       <Text style={styles.listTitle}>Location:</Text>
       <Text style={styles.listTitle}>New York, USA</Text>
       </View> 
   <View style={styles.listItem}>
   <Button
  title="Logout"
  type="solid"
  onPress={() => navigation.navigate('Login')}
  containerStyle={styles.btn}
/>
       </View> 
   
</View>
            </ImageBackground>
        </SafeViewArea>
    )
}

export default UserProfileScreen

const styles = StyleSheet.create({
    bgImage: {
        resizeMode: "contain",
        backgroundColor: "#35317E",
        width: "100%",
        flex:1,
      },
      profileSec: {
        flex:0.4,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fff",
      },
      profileTitle:{
          fontSize:18,
          color:"#35317E",
          fontWeight:'bold',
          marginVertical: 8,
      },
      profileSubTitle:{
          fontSize:14,
          color:"#844CA3",
          fontWeight:'normal',
      },
      listing:{
        flex:0.6,
        width:Dimensions.get('window').width,
        marginVertical: 40,
      },
      listItem:{
        flexDirection:'row',
        justifyContent: "space-between",
        marginHorizontal:60,
        marginVertical: 10,
        paddingBottom:5,
        borderBottomWidth:1,
        borderBottomColor:'#fff',
      },
      listTitle:{
        fontSize:18,
        color:"#ffffff",
        fontWeight:'normal',
      },
      btn:{
          width:"100%"
      }
})

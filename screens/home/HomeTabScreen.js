import React, { useEffect, useState } from 'react'
import { StyleSheet, View,ScrollView } from 'react-native'
import { Card, Button,Text } from 'react-native-elements';
import SafeViewArea from '../../components/SafeViewArea';
import axios from "../../services/axios";
import requests from "../../services/requests";

const HomeTabScreen = ({navigation}) => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const request = await axios.get(requests.fetchProduct);
        // console.log(request.data)
        setProducts(request.data);
        // return request.data;
      } catch (error) {
        console.error(error);
      }
    }

    fetchData();

  }, []);

    return (
        <SafeViewArea >
      <ScrollView>
      <View style={styles.row}>
      {products.map(product =>(
      
        <View style={styles.col} key={product.id}>
          {product.images.slice(0,1).map(images =>(
        <Card.Image key={product.id} source={{uri: images.src}}>
         
        </Card.Image>
         ))}
    <Text style={{marginBottom: 10, marginTop: 20 }} h5>{product.name}}</Text>
    <Text style={styles.price} h6>$ {product.price}</Text>
    <Button
    type="outline"
    title='Buy now'
    onPress={() => navigation.navigate('ProductDetail',{
      id: product.id,
      Name:product.name
    })}
    />

        </View>
        ))}
    </View>

      </ScrollView>
     
        </SafeViewArea>
    )
}

export default HomeTabScreen

const styles = StyleSheet.create({
    row: {
        flex: 1,
        flexWrap:'wrap',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        backgroundColor:"#ffffff",
    },
    col: {
        width:'50%',
     paddingHorizontal:20,
     marginBottom:20,
    },
})

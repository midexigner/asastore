export default {
    fetchFavicon:`/wp/v2/favicon`,
    fetchLogo:`/wp/v2/logo`,
    fetchProduct:`/wc/v3/products?consumer_key=ck_1045003307612da71f45bc811963813f4d3019a4&consumer_secret=cs_78932d2b22e966e41180bb1bb6af53dd520c1fc9&per_page=8`,
    fetchWeekly:`/mi/v2/weekly-posts`,
    fetchPages:`/wp/v2/pages`,
    fetchMedia:`/wp/v2/media`,
    fetchCategories:`/wp/v2/categories`,
    fetchTags:`/wp/v2/tags`,
    fetchUsers:`/wp/v2/users`,
    fetchComments:`/wp/v2/comments`,
    fetchSearch:`/wp/v2/search`,


}